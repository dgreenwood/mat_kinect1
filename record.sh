#!/bin/bash
'/usr/local/Cellar/libfreenect/HEAD/bin/fakenect-record' $1 &
FIND_PID=$!
SECS=$2
while [[ 0 -ne $SECS ]] && [[ $? -lt 128 ]]
do
    sleep 1
    SECS=$[$SECS-1]
done

kill -2 $FIND_PID



clear all; clc;
timeout = 5;
fprintf('Start recording from the kinect:\n');
fprintf('Press Ctrl+C to stop or timeout after %d seconds\n',timeout);

output = '~/out/temp';

system(sprintf('rm -R %s',output));

[SUCCESS,MESSAGE,MESSAGEID] = mkdir(output);

if MESSAGEID
    error('RECORDING NOT MADE: %s %s', output, MESSAGE)
else
    [~, result]=system(sprintf('%s/record.sh %s %d', ...
                    pwd,output, timeout));

    disp('Recording finished:')
    disp(result(1:100))
    disp(result(end-26:end))

    ppm = dir( fullfile(output,'*.ppm') );   %# list all *.ppm  files
    pgm = dir( fullfile(output,'*.pgm') );   %# list all *.pgm  files
    % dmp = dir( fullfile(output,'*.dump') );  %# list all *.dump files
end

%save recording to .mat files:

FigHandle = figure();
set(FigHandle, 'Position', [100, 100, 1400, 600]);

for i = 1:length(pgm)-5
    depth = imread(sprintf('%s/%s', output, pgm(i).name));
    depth = swapbytes(depth);
    rgb = imread(sprintf('%s/%s', output, ppm(i).name));
    rgb = swapbytes(rgb);
    subplot(1,2,1), imagesc(depth); axis image;
    subplot(1,2,2), image(rgb); axis image;
    drawnow
end
